CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Features
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Eventbrite Attendees module adds a new block to the system for showing a
list of attendees to an Eventbrite event.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/eventbrite_attendees

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/eventbrite_attendees


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

 * Eventbrite OAuth token - for more information, visit:
   https://www.eventbrite.com/developer/v3/api_overview/authentication/


FEATURES
--------

 * Template on the attendees-list level, and custom template suggestion through
   UI
 * Cache JSON response list of attendees
 * Token replacement for contextual node when placed on a node route


INSTALLATION
------------

 * Install the Eventbrite Attendees module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Web services > Eventbrite
       Attendees settings and provide your Personal OAuth Token.
    3. Save configuration.

Configure Block:

    1. Navigate to Administration > Structure > Block layout and add an
       Eventbrite Attendees block.
    2. Provide the specific Eventbrite event ID, or a token from current node
       being viewed.
    3. Select the cache JSON response time, if this event is over, there is no
       need to continue querying against the API.
    4. Save Block.


MAINTAINERS
-----------

 * Jonathan Daggerhart (daggerhart) - https://www.drupal.org/u/daggerhart
